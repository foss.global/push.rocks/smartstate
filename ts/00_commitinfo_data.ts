/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartstate',
  version: '2.0.6',
  description: 'a package that handles state in a good way'
}
